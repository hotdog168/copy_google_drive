Vancouver Community Network
 Warren Holley

A suite of small programs to copy one Google Drive file tree to another.
Intended for single use, as there exist cases that the drive does not currently catch.

REQUIREMENTS: 
For the source file tree, for each folder, each file in the folder must have a unique name.
-- There is a script in the suite to find duplicate names in the folder.

The target must be the more recent of the files. This is not RSync.
The source is copied only if there is no file with that name in the target.

Finally, the executor must have edit and existance control of the files being copied.
Non-editable files, or files that cannot be deleted or moved, WILL NOT be copied.


TO RUN THE CODE:
Access Google Drive, right click the main page contents.
Under 'More', select 'Google App Script'. A new page will open.

Copy and paste the two '.gs' files in the Git to the folder in two seperate subfiles/subscripts (under 'File/New/Script File')

Then you must configure the Folder Pointers.
In the scipt file that holds the 'Main Function.gs' Script Code, there are two variables labeled
inFolderId and outFolderId.
When in Google Drive, when viewing a folder, the value you want to put in these variables is the end-part of the URL.
EG: 
https://drive.google.com/drive/u/1/folders/##XXxxXx##x#x####xxXx
the ID of the folder is this part:         ##XXxxXx##x#x####xxXx
Copy and paste that ID string to the folder Variables.

To run the code: 
While viewing the Script containing the 'Main Function.gs' code, on the top bar
click 'Select Function', then pick the function you want to execute.




Please note that this code is preliminary, and slow as hell.
The speed is not an issue of the code, but how Google Servers handle it.

Please note that there is a 'Logger' in the scripting IDE.
It is under 'View' in the opetions bar.
This is only updated on the exiting of the code. (Completion or Error)



If there are any issues with the code, please leave an Issue in the Issue Tracker.
