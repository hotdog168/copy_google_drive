// Google Drive Copy Scripts
// Vancouver Community Network
// Aug 8, 2018
// Version 1.0

//Function Library.
//Intented to be called from the mainFunction.gs script, found in the sidebar.

//Primary Functions:
//  driveCopyInitiator (inFolderId, outFolderId);
//  countFiles(sourceFolderId);
//  compareFileSizes(inFolderId, outFolderId);
//  findDuplicateNames(souceFolderId);

//Utility Functions:
//  searchForFolder(folderDir,folderName);
//  searchForFile(folderDir,folderName);

//-------------------------------------------------------------------------------
//Function: driveCopy (Args: inFolderId, outFolderId)

// Simple copy. Copies from one folder to another.
// Does not check for changes in the file (Yet, TODO)
// Does not overwrite. Cannot not copy complex/looping/multi-parent hierarchies.

//NOTE: Will break if the person running the code cannot edit a file to be copied.
//Does not currently edit description, owner, editors, etc. (Yet, TODO).

var sourceDirCount = 0;
var sourceFilesCount = 0;

var destDirCount = 0;
var destFilesCount = 0;

var dirsCopiedCount = 0;
var filesCopiedCount = 0;

var copyErrorCount = 0;

function driveCopyInitiator(inFolderId, outFolderId) {
  //Reset Counters.
  sourceDirCount = 0;
  sourceFilesCount = 0;
  destDirCount = 0;
  destFilesCount = 0;
  dirsCopiedCount = 0;
  filesCopiedCount = 0;
  copyErrorCount = 0;
  
  var inFolder = DriveApp.getFolderById(inFolderId); 
  var outFolder = DriveApp.getFolderById(outFolderId);
  
  driveCopyRecurse(inFolder,outFolder);
  
  Logger.log("\n\n");
  Logger.log("-FINAL STATS-");
  Logger.log("Source Dir Count:"+sourceDirCount);
  Logger.log("Source File Count:"+sourceFilesCount+"\n");
  
  Logger.log("Dest Dir Count:"+destDirCount);
  Logger.log("Dest File Count:"+destFilesCount+"\n");
  
  Logger.log("Dirs Copied Count:"+dirsCopiedCount);
  Logger.log("Files Copied Count:"+filesCopiedCount+"\n");
  
  Logger.log("Copy Error Count:"+copyErrorCount);
}

//Recursive, depth first folder copy, followed by a local file copy.
function driveCopyRecurse (sourceFolder, destFolder) {
  var sourceDirList = sourceFolder.getFolders();
  var destDirList = destFolder.getFolders();
  
  sourceDirList = sourceFolder.getFolders();// Reset
  while (sourceDirList.hasNext()) {
    
    var toCopyFolder = sourceDirList.next();
    sourceDirCount++;
    
    var targetFolder = searchForFolder(destFolder,toCopyFolder.getName());
    
    if (targetFolder == 0) {
      targetFolder = destFolder.createFolder(toCopyFolder.getName());
      dirsCopiedCount++;
    }
    destDirCount++;
    driveCopyRecurse(toCopyFolder,targetFolder);
  }
  
  var sourceFileList = sourceFolder.getFiles();
  while (sourceFileList.hasNext()) {
    var inFile = sourceFileList.next();
    sourceFilesCount++;
    
    var outFile = searchForFile(destFolder,inFile.getName());
    if (outFile == 0) {
      try {
        outFile = inFile.makeCopy(inFile.getName(),destFolder);
        filesCopiedCount++;
        destFilesCount++;
      } catch (e) {
        Logger.log("! COPY ERROR - " + sourceFolder.getName() + "/" + inFile.getName() + " - " + e); //Update to include a 'path' arg like in compareFileSizes();
        copyErrorCount++;
      }
    }
    else {
      destFilesCount++;
    }
  } 
}

//-----------------------------------------------------------------------------------------------------------

// Logs a count of the number of files in the system.
// Note: Due to delays in Google Drive, may not give accurate number immediately after a recent change.
function countFiles(sourceFolderId) {
  Logger.log("File Count: "+recurseCount(DriveApp.getFolderById(sourceFolderId))); //Team Help Desk
}
function recurseCount (sourceFolder) {
  var localFileCount = 0;
  var recurseFileCount = 0;
  
  var folderList = sourceFolder.getFolders();
  while (folderList.hasNext()) {
    var tempFolder = folderList.next()
    recurseFileCount += recurseCount(tempFolder);
  }
  
  var localFileList = sourceFolder.getFiles();
  while (localFileList.hasNext()) {
    var tempFile = localFileList.next();
    localFileCount++;
  }
  return localFileCount + recurseFileCount;
}

//----------------------------------------------------------------------------------------------------------

// For each folder in inFolder, recurse to the folder and matching folderName in the outFolder.
// Then, for each file in the inFolder, find the matching file in the outFolder, and compare sizes.
// DOES NOT THROW ERRORS. Alerts to the Logger if files/folders are not found or file sizes are mismatched.
function compareFileSizes(inFolderId, outFolderId) {
  var inFolder = DriveApp.getFolderById(inFolderId);
  var outFolder = DriveApp.getFolderById(outFolderId);
  
  //For each file, find appropriate file in the outfolder, compare lengths.
  recurseCompareSizes(inFolder,outFolder, "Root:"+inFolder.getName()); 
}
function recurseCompareSizes(inFolder, outFolder, path) {
  var inFolderFolderList = inFolder.getFolders();
  while (inFolderFolderList.hasNext()) {
    var inFolderFolder = inFolderFolderList.next();
    
    var outFolderFolder = searchForFolder(outFolder,inFolderFolder.getName());
    if (outFolderFolder == 0) {
      Logger.log("!ERROR - Folder Not Found in OutFolder" + path + "/" + inFolderFolder.getName());
    }
    else {
      recurseCompareSizes(inFolderFolder,outFolderFolder, path + "/"+inFolderFolder.getName());
    }    
  }
  
  var inFolderFileList = inFolder.getFiles();
  while (inFolderFileList.hasNext()) {
    var inFolderFile = inFolderFileList.next();
    var outFolderFile = searchForFile(outFolder,inFolderFile.getName());
    
    
    
    if (outFolderFile == 0) {
      Logger.log("!ERROR - File Not Found in OutFolder: " + path + "/" + inFolderFile.getName());
      continue;
    }
    
    //Comparing Blob byte lengths as each file is almost always 0Bytes on Google Drive,
    // comparing Blobs does not work as they are objects,
    // and only by comparing byte lengths does this work.
    // Could upgrae to compare blobs byte-for-byte, though would be far slower.
    var inFileSize  = inFolderFile.getBlob().getBytes().length;
    var outFileSize = outFolderFile.getBlob().getBytes().length;
          
    if (inFileSize != outFileSize) {
      Logger.log("!ERROR - Mismatched Size: "+path+"/"+inFolderFile.getName());
      Logger.log("in: "+ inFileSize + "  out: " + outFileSize);
    }
    else {
      Logger.log("  Good - Size Match: "+path+"/"+inFolderFile.getName());
      //Logger.log("  in: "+inFileSize + "  out: " + outFileSize);
    }
  }
}

//--------------------------------------------------------------------------------------------------------

// Function - findDuplicateNames (Arg: ID of source Folder);
// Steps through the source folder and it's folders looking for files within a folder with the same name.
// Intended to debug issues when doing Name-Based copies.
function findDuplicateNames(sourceFolderId) {
  var sourceFolder = DriveApp.getFolderById(sourceFolderId); //From VCN-Documents
  findDuplicateNamesRecurse(sourceFolder,"Start/");
}

function findDuplicateNamesRecurse(sourceFolder, path) {
  var folderList = sourceFolder.getFolders();
  var fileList = sourceFolder.getFiles();
  
  while (folderList.hasNext()) {
    var tempFolder = folderList.next();
    findDuplicateNamesRecurse(tempFolder,path+tempFolder.getName()+"/");
  }
  while (fileList.hasNext()) {
    var fileA = fileList.next();
    
    var comparisonList = sourceFolder.getFiles();
    while (comparisonList.hasNext()) {
      var fileB = comparisonList.next();
      if (fileA.getId() != fileB.getId() && fileA.getName() == fileB.getName()) {
        Logger.log("DUPLICATE: " + path + fileA.getName());
        break;
      }
      
    }
  }
}


//----------------------------------------------------------------------------------------------------------

//Functions: searchForFolder (Args: Parent Folder, Target Folder Name)
//           searchForFile   (Args: Parent Folder, Target File Name)
// Returns a Folder/File object if there exists a folder/file with the name given.
// For finding a file/folder within a folder easily.
// Returns 0 (int Zero) if no folder is found. (Nulls were giving undiagnosed issue)

function searchForFolder(folderDir,folderName) {
  var dirFolderFolders = folderDir.getFolders();
  while (dirFolderFolders.hasNext()) {
    var tempFolder = dirFolderFolders.next();
    if (folderName == tempFolder.getName()) {
      return tempFolder;
    }
  }
  return 0;
}

function searchForFile(folderDir,fileName) {
  var dirFolderFiles = folderDir.getFiles();
  while (dirFolderFiles.hasNext()) {
    var tempFile = dirFolderFiles.next();
    if (fileName == tempFile.getName()) {
      return tempFile;
    } 
  }  
  return 0;
}
